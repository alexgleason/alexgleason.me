import { getLastModified } from './git';

interface FileInfo {
  lastModified: string
  sourceUrl: string
}

const getFileInfo = (filename: string): FileInfo => {
  return {
    lastModified: getLastModified(filename),
    sourceUrl: `https://gitlab.com/alexgleason/alexgleason.me/-/blob/main/${filename}`,
  };
};

export {
  getFileInfo,
};