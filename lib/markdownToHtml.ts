import { remark } from 'remark';
import html from 'remark-html';

export default function markdownToHtml(markdown: string): string {
  const result = remark()
    .use(html, { sanitize: false })
    .processSync(markdown);

  return result.toString();
}
