import manifest from 'cryptocurrency-icons/manifest.json';

interface Coin {
  symbol: string
  name: string
  color: string
}

interface CoinDB {
  [symbol: string]: Coin | undefined
}

const manifestMap = manifest.reduce<CoinDB>((result, entry) => {
  result[entry.symbol.toLowerCase()] = entry;
  return result;
}, {});

/** Get title from CoinDB based on ticker symbol */
export const getTitle = (ticker: string): string => {
  return manifestMap[ticker]?.name || '';
};

export default manifestMap;