---
title: 'Parler’s epic fail: A crash course on running your own servers on a shoestring budget'
excerpt: In the early days of 2021 we learned the true power of American tech companies. After Trump was ousted by Twitter, the social media alternative Parler was cut off by their cloud hosting provider, Amazon.
coverImage: '/assets/images/poweredge.png'
date: '2021-02-10T21:06:54.000Z'
author:
  name: Alex Gleason
  picture: '/assets/avatars/alex.png'
ogImage:
  url: '/assets/images/poweredge.png'
---

> First they came for the Communists,  
> and I didn’t speak up,  
> because I wasn’t a Communist.  
> Then they came for the Jews,  
> and I didn’t speak up,  
> because I wasn’t a Jew.  
> Then they came for the Catholics,  
> and I didn’t speak up,  
> because I was a Protestant.  
> Then they came for me,  
> and by that time there was no one  
> left to speak up for me.  

In the early days of 2021 we learned the true power of American tech companies. After Trump was ousted by Twitter, the social media alternative Parler was cut off by their cloud hosting provider, Amazon.

Where do we go from here? What can a freedom-minded person do to avoid censorship by tech oligarchs?

The only way is to meet them at their level. In some industries it's impossible to challenge the establishment, but anyone can have a voice on the Internet. This is still true in 2021.

## Cloud hosting vs real computers

Cloud hosting has made it easy for anyone to run a website. It lowered the barrier of entry, improved tech literacy, and I wouldn't be where I am today without it. But it's time to cut out the middleman.

You can run a real server anywhere there's power and an internet connection. You can run a server from your home. You can run it from someone else's home. You can run it from a datacenter in an undisclosed location.

<figure>
  <img src='/assets/images/piratebay.jpg' />
  <figcaption>Servers for The Pirate Bay at an undisclosed location.</figcaption>
</figure>

It's true an ISP can censor, but it's a whole different playing field. You can find datacenters run by Conservative Boomers, Russians, porn companies, and principled free speech advocates. By the time it becomes a bigger problem, I'm sure some of us will be [running our own ISP](https://startyourownisp.com/), too.

It's a great ability to be able to pick up and move, plug in somewhere else, and continue to stay online. I argue that your chances of survival are much better this way, and Parler is foolish for not going this route. We can do better.

## Getting started with a computer on your home internet

Our mission is to run a high-availability production server in a datacenter. But first, we should should experiment with a server at home. Here's what you will need to achieve it:

- An internet connection
- A computer running 24/7
- A router that lets you configure port forwarding
- Ethernet cables

For higher availability, you may consider:

- Battery backup
- Wireless 4G backup connection
- Fiber-optic internet connection

<figure>
  <img src='/assets/images/piratebay-server.png' />
  <figcaption>The 'first' Pirate Bay server, on display at the Computer Museum in Linköping, Sweden.</figcaption>
</figure>

Running a server at home is a good starting point. It could serve as a testing facility, production backup, or even serve a small online community. Chances are, you already have all the equipment needed.

Check out [/r/homelab](https://www.reddit.com/r/homelab/) on Reddit for inspiration.

It is critical you have fast enough **upload** speeds if you're serving real users. Check out [speedtest.net](https://www.speedtest.net/) to find out.

With upload speeds as low as 5 Mbps, you may be able to host an online community with 20,000 users. Keep in mind your video calls will compete for upload bandwidth on the home network.

If you score a fiber internet connection, that is really good, as it can give up to 1 Gbps _upload_ speeds. That's quite literally datacenter levels of bandwidth at your home.

With this setup, you must be able to accept power outages and potential network failures. A battery backup and wireless internet backup can help with this.

Some people have taken this concept very far, building literal datacenters in their homes.

<figure>
  <img src='/assets/images/home-datacenter.jpg' />
  <figcaption>Home datacenter by Québec, Canada IT Teacher <a href='https://ve2cuy.wordpress.com/my-home-data-center/' target='_blank'>VE2CUY</a>.</figcaption>
</figure>

To make your computer accessible from the outside world, you can configure a port forward in your router. This will allow users to visit your public IP address to access your home server. You may then accordingly set up some domain names for your public IP.

<figure>
  <img src='/assets/images/firewall-port-forwards.png' />
  <figcaption>My home router, forwarding ports 80 and 443 to my server's local IP.</figcaption>
</figure>

Some ISPs offer expensive business plans that give you a static IP address, but I just used my dynamic IP. It seems to change rarely, if ever.

## Obtaining a rack server

When running a home server isn't enough, you'll need to move into a datacenter.

Before getting into a datacenter, you'll need a computer than can fit into a standard rack. Rack servers are very similar to desktops, but come in a form factor of 1U, 2U, 3U, and 4U.

1U describes the height. 2U is the same height as two 1U servers, and so on. A typical rack at a datacenter is 42U, so it can fit fourty-two 1U servers, or twenty-one 2U. The depth of a server is also variable, but that doesn't really matter unless you're buying a rack. Servers are all the same width.

The server I chose is a [Dell PowerEdge R720](https://downloads.dell.com/manuals/all-products/esuprt_ser_stor_net/esuprt_poweredge/poweredge-r720_reference-guide_en-us.pdf), which I purchased for $450 off eBay. This 2U server has 32 vCores, 64 GB of RAM, and a RAID interface with 8 HDD slots.

<figure>
  <img src='/assets/images/poweredge-cropped.jpg' />
  <figcaption>My Dell PowerEdge R720 connected to my home internet.</figcaption>
</figure>

It connects to the wall with standard power cables. There are 4 Ethernet ports, two USB ports, and a VGA input for attaching a screen.

Note that it's a lot bigger than you think! Make sure you have space for it.

I chose a PowerEdge because they're beloved by hobbyists, and a lot of information exists about them online. My model is from 2012, which seems to be a sweet spot between power and affordability. These machines are built to last a very long time.

Other models from that era are equally good. For a 1U option, consider a PowerEdge R320. To understand the naming convention: the R stands for "rack", while the R3xx and R7xx are 1U and 2U products respectively. The [newest PowerEdge models right now](https://www.dell.com/en-us/work/shop/dell-poweredge-servers/sc/servers/poweredge-rack-servers) are the R340 and R740.

You should run this server from your home internet at first, while you get things up and running.

## Installing hard drives

For obvious reasons, hard drives were not included. Right now, refurbished 4TB SATA HDDs have the best cost per Gigabyte. I bought some off Amazon. HGST brand is a good bet, as they are inexpensive refurbished Western Digital drives with a different label.

<figure>
  <img src='/assets/images/sata-vs-sas.jpg' />
  <figcaption>SATA drive on top, SAS on the bottom.</figcaption>
</figure>

It's worth noting most servers support SAS drives also. SAS is an alternative to SATA with better reliability and a different performance profile. The connector in my R720 is actually a SAS connector, but SATA drives fit into them and work just fine.

I went with SATA for my build because of better price and availability. Just know it's an option, and don't mix and match them while doing RAID.

## Differences in RAM

My PowerEdge uses DDR3 RDIMM memory. I should note that UDIMM and LDIMM are also options. Traditional desktop computers use UDIMM (unbuffered memory), while servers have an option between 3 different types. You must not mix different types in the same computer.

<figure>
  <img src='/assets/images/rdimm-diagram.png' />
  <figcaption>Registered memory (RDIMM)</figcaption>
</figure>

RDIMM (registered memory) is the most common type of memory for servers. Each stick contains an extra chip (a register) used to reduce load to the memory controller, letting your hardware last longer.

When upgrading RAM, you should also pay special attention to the **rank** of the memory. Rank is usually a number between 1 and 4. Your server has a maximum allowed rank per channel, so if the total rank is too high you might have to leave some slots empty.

## Installing an OS

For the OS, I cannot recommend [Proxmox VE](https://www.proxmox.com/en/proxmox-ve) enough. It allows you to easily create virtual machines without much knowledge or pain.

<figure>
  <img src='/assets/images/proxmox-screenshot.png' />
  <figcaption>Proxmox VE running on my server.</figcaption>
</figure>

I recommend this for a first server because of its flexibility, and you can always add bare metal nodes to the network later. All virtual machines appear as first-class citizens on the host network, so you can easily network them with physical nodes.

## Further learning

YouTube has been a godsend for helping me learn all this stuff and set it up. Here are a few recommendations:

- [Manually Updating the Firmwares on a Dell PowerEdge R610](https://www.youtube.com/watch?v=86KK_dZoCJ0) by Rob Willis
- [Switching and Routing](https://www.youtube.com/watch?v=ck3gx9HB9-k&list=PLSNNzog5eydtmcbcbc1b8pVRkgre3vNUy) by Sunny Classroom
- [Data Center NETWORKS (what do they look like??)](https://www.youtube.com/watch?v=6-66D9J5PkY) by NetworkChuck
- [How to Virtualize Your Home Router / Firewall Using pfSense](http://web.archive.org/web/20221207030253/https://www.youtube.com/watch?v=hdoBQNI_Ab8) by Techno Tim
- [A DAY in the LIFE of the DATA CENTRE | RACKING SERVERS with ASH & JAMES!](http://web.archive.org/web/20221207030253/https://www.youtube.com/watch?v=wFCc3ts74pQ) by Custodian Data Centres

## Installing your server at a local datacenter

Once your rack server is all set up and ready to go, it's time to move it to a datacenter.

Some datacenters are privately owned by a single company like Google, but many others are independent companies that lease their racks to anyone willing to pay. This is called _colocation_.

<figure>
  <img src='/assets/images/data-foundry.jpg' />
  <figcaption><a href='https://www.datafoundry.com/' target='_blank'>Data Foundry</a>, an independent Houston datacenter offering colocation.</figcaption>
</figure>

If you live near a metropolitan area, chances are there's a datacenter nearby willing to let you install your own server. You will get access to power, high speed internet, and multiple layers of security.

Try searching "&#60;my city&#62; datacenter" or "&#60;my city&#62; ISP".

If they offer it, I recommend scheduling a tour with a local datacenter. It's a lot of fun and will give you a better idea of what you're dealing with.

Most datacenters only lease by the rack. These racks can hold up to 42 servers and are far too expensive. What you need is a company that lives _inside_ the datacenter and is willing to lease you space _within_ a single rack. You are looking for something like this:

<figure>
  <img src='/assets/images/layerhost-colo.png' />
  <figcaption>Colo pricing from <a href='https://www.layerhost.com/houston-colocation' target='_blank'>LayerHost</a> in Houston. LayerHost is located inside of Data Foundry.</figcaption>
</figure>

When I installed my server at the datacenter, it went something like this:

1. I purchased a plan from the website.
2. The datacenter provided me network information over email, including a static IPv4 block, which I used to configure my server's network.
3. Using support tickets, we scheduled a time to install the server.
4. On that day, I drove to the datacenter and parked. I sent a text message to the provided phone number and a technician came outside. We wheeled my server in on a cart, through some doors, into the rack area.
5. The technician installed my server on the rack and connected it to the network.
6. I made final changes and tested that I could access my server from outside.
7. That's it! Voilà, you're in a datacenter.

While this may seem daunting, it is a necessary next step for freedom online. I hope that more people will follow this path of running their own hardware.

Money spent so far:

- $450 - Used PowerEdge R720 server
- $500 - 2x 4TB HDDs, 2x 12TB HDDs
- $13 - A DVD drive for the server
- $25 - A replacement network card for the server (just in case)
- $200 - First month hosting, incl. a static IPv4 block

This makes my startup costs a grand total of **$1,188**. However, the recurring cost is now only **$200/mo**, and I intend to make a lot more than that through the sites I'm hosting.

I am now running [gleasonator.com](https://gleasonator.com/) off this server, and it feels GREAT to not live in fear. Soon I will be migrating more sites over, and adding extra servers for high availability/redundancy.

I am also developing a social hosting service called [Tribes.host](https://tribes.host/). Tribes will allow anyone to host their own social media site on the [Fediverse](https://jointhefedi.com/) affordably, running off my server hardware. We are in alpha stage at the moment, but let's see how far we can take this! I'm eager to give big social media sites like Facebook and Twitter a run for their money.

I hope this post inspired you to invest in internet infrastructure. Where there's a will there's a way, and the will seems stronger now than ever. Run your own server today!