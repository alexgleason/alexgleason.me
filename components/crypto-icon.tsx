/** Get crypto icon URL by ticker symbol, or fall back to generic icon */
const getIcon = (ticker: string): string => {
  try {
    return require(`cryptocurrency-icons/svg/color/${ticker.toLowerCase()}.svg`).default.src;
  } catch {
    return require('cryptocurrency-icons/svg/color/generic.svg').default.src;
  }
};

interface ICryptoIcon {
  ticker: string,
  title?: string,
  className?: string,
}

const CryptoIcon: React.FC<ICryptoIcon> = ({ ticker, title, className }) => {
  return (
    <div className={className}>
      <img
        src={getIcon(ticker)}
        alt={title || ticker}
      />
    </div>
  );
};

export default CryptoIcon;
