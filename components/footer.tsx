import Container from './container';
import SmartLink from './smart-link';

const Footer = () => {
  return (
    <footer className='bg-neutral-50 border-t border-neutral-200 pb-16'>
      <Container>
        <div className='py-12 lg:flex lg:space-x-36 space-y-12 lg:space-y-0'>
          <div className='space-y-4 xl:space-y-8'>
            <div className='xl:flex xl:space-x-8 space-y-4 xl:space-y-0'>
              <img
                className='w-16 h-16'
                src='/assets/logo.svg'
                alt='Alex Gleason'
              />

              <p className='text-2xl max-w-sm'>
                Alex Gleason for the Internet. Keep the web open and free.
              </p>
            </div>

            <p className='text-gray-500'>
              ♡{new Date().getFullYear()} Alex Gleason. Please <a className='underline' href='https://gitlab.com/alexgleason/alexgleason.me' target='_blank'>copy and share</a>.
            </p>
          </div>

          <FooterCategory title='Contact'>
            <FooterLink href='mailto:alex@alexgleason.me'>Email: alex@alexgleason.me</FooterLink>
            <FooterLink href='https://gleasonator.com/@alex'>Fediverse: @alex@gleasonator.com</FooterLink>
            <FooterLink href='https://njump.me/npub1q3sle0kvfsehgsuexttt3ugjd8xdklxfwwkh559wxckmzddywnws6cd26p'>Nostr: @alex@gleasonator.dev</FooterLink>
          </FooterCategory>

        </div>
      </Container>
    </footer>
  );
};

interface IFooterLink {
  href: string
  children: React.ReactNode
}

const FooterLink: React.FC<IFooterLink> = ({ href, children }) => {
  return (
    <SmartLink href={href} className='block'>
      {children}
    </SmartLink>
  );
};

interface IFooterCategory {
  title: string
  children: React.ReactNode
}

const FooterCategory: React.FC<IFooterCategory> = ({ title, children }) => {
  return (
    <div className='space-y-5'>
      <h3 className='text-2xl font-semibold'>{title}</h3>
      <div className='space-y-4'>
        {children}
      </div>
    </div>
  );
};

export default Footer;
