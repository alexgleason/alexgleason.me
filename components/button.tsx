import clsx from 'clsx';

import SmartLink from './smart-link';

interface IButton {
  href: string
  theme: 'primary' | 'secondary' | 'outline'
  children: React.ReactNode
  className?: string
  group?: boolean
  size?: 'sm' | 'lg'
  icon?: React.ReactNode
}

const Button: React.FC<IButton> = ({ href, theme, children, className, group, size = 'lg', icon }) => {
  return (
    <SmartLink
      href={href}
      className={clsx(
        className,
        'rounded-full',
        'inline-block focus:outline-none focus:ring-2 focus:ring-offset-2 appearance-none transition-all',
        'no-underline decoration-transparent',
        {
          'group': group,
          'bg-azure': theme === 'primary',
          'bg-red-600': theme === 'secondary',
          'border-2 border-white': theme === 'outline',
          'text-lg py-4 px-8': size === 'lg',
          'text-sm py-2 px-4': size === 'sm',
        },
      )}
    >
      <span
        className={clsx('no-underline flex items-center', {
          'space-x-1': size === 'sm',
          'space-x-2': size === 'lg',
          'text-white': ['primary', 'secondary'].includes(theme),
        })}
      >
        <span>{children}</span>
        {icon}
      </span>
    </SmartLink>
  );
};

export default Button;