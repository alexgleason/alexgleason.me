import clsx from 'clsx';
import throttle from 'lodash/throttle';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useCallback, useEffect, useState } from 'react';

import logo from '../public/assets/logo.svg';

import Container from './container';
import MenuButton from './menu-button';


const Navbar: React.FC = () => {
  const [menuOpen, setMenuOpen] = useState(false);

  const toggleMenu = () => setMenuOpen(!menuOpen);
  const closeMenu = () => setMenuOpen(false);

  const handleResize = useCallback(throttle(() => {
    if (window.innerWidth >= 1024) {
      closeMenu();
    }
  }, 50), []);

  useEffect(() => {
    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  useEffect(() => {
    document.documentElement.classList.toggle('overflow-hidden', menuOpen);
  }, [menuOpen]);

  return (
    <nav className={clsx('flex flex-col z-50 sticky top-0 backdrop-blur overflow-hidden transition-all', {
      'bg-white/95': !menuOpen,
      'bg-white/95 backdrop-blur-md h-screen': menuOpen,
    })}
    >
      <Container>
        <div className='py-2 lg:py-4 space-x-10 flex justify-between items-center'>
          <Link href='/'>
            <img
              alt='Logo'
              src={logo.src}
              className='h-20'
            />
          </Link>

          <div>
            <div className='hidden lg:flex items-center space-x-10'>
              <NavbarLink href='/work'>
                Work
              </NavbarLink>

              <NavbarLink href='/donate'>
                Donate
              </NavbarLink>

              <NavbarLink href='/blog'>
                Blog
              </NavbarLink>
            </div>

            <div className='lg:hidden text-gray-600'>
              <MenuButton
                active={menuOpen}
                onClick={toggleMenu}
              />
            </div>
          </div>
        </div>
      </Container>

      <div
        className={clsx('overflow-y-auto', {
          'hidden': !menuOpen,
        })}
      >
        <Container className='my-10 space-y-10'>
          <MobileLink href='/work' onClickCurrent={closeMenu}>
            Work
          </MobileLink>

          <MobileLink href='/blog' onClickCurrent={closeMenu}>
            Blog
          </MobileLink>

          <MobileLink href='/donate' onClickCurrent={closeMenu}>
            Donate
          </MobileLink>
        </Container>
      </div>
    </nav>
  );
};

interface INavbarLink {
  href: string
  children: React.ReactNode
}

const NavbarLink: React.FC<INavbarLink> = ({ href, children }) => {
  return (
    <Link href={href} className='block font-semibold text-sm md:text-base text-gray-600'>
      {children}
    </Link>
  );
};

interface IMobileLink {
  href: string
  children: React.ReactNode
  /** Callback when a link to the current page is clicked. */
  onClickCurrent?: React.MouseEventHandler<HTMLAnchorElement>
}

const MobileLink: React.FC<IMobileLink> = ({ href, children, onClickCurrent }) => {
  const { pathname } = useRouter();

  const handleClick: React.MouseEventHandler<HTMLAnchorElement> = (e) => {
    if (onClickCurrent && href.replace(/\/$/, '') === pathname) {
      onClickCurrent(e);
    }
  };

  return (
    <Link
      href={href}
      onClick={handleClick}
      className='block font-semibold text-2xl text-gray-600'
    >
      {children}
    </Link>
  );
};

export default Navbar;