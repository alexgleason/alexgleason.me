import { FormattedNumber as InnerFormattedNumber } from 'react-intl';

/** Check if a value is REALLY a number. */
export const isNumber = (value: unknown): value is number => typeof value === 'number' && !isNaN(value);

const roundDown = (num: number) => {
  if (num >= 100 && num < 1000) {
    num = Math.floor(num);
  }

  const n = Number(num.toFixed(2));
  return (n > num) ? n - (1 / (Math.pow(10, 2))) : n;
};

interface IFormattedNumber {
  number: unknown
  max?: number
}

const FormattedNumber: React.FC<IFormattedNumber> = ({ number, max }) => {
  if (!isNumber(number)) return <>•</>;

  let value = number;
  let factor = '';
  if (number >= 1000 && number < 1000000) {
    factor = 'k';
    value = roundDown(value / 1000);
  } else if (number >= 1000000) {
    factor = 'M';
    value = roundDown(value / 1000000);
  }

  if (max && value > max) {
    return <span>{max}+</span>;
  }

  return (
    <span>
      <InnerFormattedNumber
        value={value}
        maximumFractionDigits={0}
        minimumFractionDigits={0}
        maximumSignificantDigits={3}
        style='decimal'
      />
      {factor}
    </span>
  );
};

export default FormattedNumber;