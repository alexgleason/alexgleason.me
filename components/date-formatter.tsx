import { parseISO, format as formatDate } from 'date-fns';

type Props = {
  dateString: string
  format?: string
}

const DateFormatter = ({ dateString, format = 'EEE, LLL d, yyyy' }: Props) => {
  const date = parseDate(dateString);
  return <time dateTime={dateString}>{formatDate(date, format)}</time>;
};

function parseDate(dateString: string): Date {
  if (dateString.includes('T')) {
    return parseISO(dateString);
  } else {
    const [year, month, day] = dateString.split('-').map(Number);
    return new Date(year, month - 1, day);
  }
}

export default DateFormatter;
