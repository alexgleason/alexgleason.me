import Container from '../components/container';
import CryptoAddress from '../components/crypto-address';
import Layout from '../components/layout';
import PageTitle from '../components/page-title';
import UnifiedMeta from '../components/unified-meta';
import Wrapper from '../components/wrapper';

interface Wallet {
  ticker: string
  address: string
  note?: string
}

interface Props {
  wallets: Wallet[]
}

export default function DonatePage({ wallets }: Props) {
  return (
    <Layout>
      <UnifiedMeta
        title='Donate | Alex Gleason'
        description='Help me fight big tech with a donation!'
      />
      <Container>
        <Wrapper>
          <div className='my-16 space-y-8'>
            <PageTitle>Support Alex Gleason</PageTitle>

            <p className='text-xl'>
              Join my fight to preserve freedom online!
              Your donation will help me to keep hacking and keep advocating.
            </p>
          </div>
        </Wrapper>

        <div className='max-w-2xl mx-auto my-20'>
          <PaymentService
            image='/assets/proprietary/PayPal.svg'
            url='https://www.paypal.com/donate/?business=VH66DMWWK4UKS&no_recurring=0&item_name=Thank+you+for+your+support%21&currency_code=USD'
          />
        </div>

        <Wrapper>
          <div className='grid gap-5 my-20'>
            {wallets.map(wallet => (
              <CryptoAddress key={wallet.address} {...wallet} />
            ))}
          </div>
        </Wrapper>
      </Container>
    </Layout>
  );
}

interface IPaymentService {
  image: string
  url: string
}

const PaymentService: React.FC<IPaymentService> = ({ image, url }) => {
  return (
    <a
      className='flex p-8 justify-center items-center border rounded-lg'
      href={url}
      target='_blank'
    >
      <img
        className='h-12 max-w-full object-contain'
        src={image}
      />
    </a>
  );
};

export const getStaticProps = async () => {
  const response = await fetch('https://gleasonator.com/api/pleroma/frontend_configurations');
  const feConfig = await response.json();
  const wallets: Wallet[] = feConfig.soapbox_fe.cryptoAddresses;

  return {
    props: { wallets },
  };
};
