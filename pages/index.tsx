import { IconExternalLink } from '@tabler/icons';

import Button from '../components/button';
import Container from '../components/container';
import Layout from '../components/layout';

export default function Homepage() {
  return (
    <Layout>
      <div className='relative'>
        <div className='absolute left-0 right-0 top-0 h-96 bg-gradient-to-b from-white -z-40' />

        <div
          className='bg-cover absolute w-full h-full opacity-90 -z-50'
          style={{ backgroundImage: 'url(/assets/images/skyline.jpg)' }}
        />

        <Container className='lg:flex'>
          <div className='my-20 md:my-36 space-y-16'>
            <h1 className='text-azure text-5xl sm:text-6xl md:text-7xl font-extrabold leading-tight md:leading-none text-center lg:text-left'>
              <span>Join my fight</span>
              <br />
              <span className='text-red-600'>against big tech</span>.
            </h1>
            <Button theme='secondary' href='/work' icon={<IconExternalLink size={20} />}>
              View My Work
            </Button>
          </div>

          <Standee src='/assets/images/alex-crossed.png' />
        </Container>
      </div>

      <div
        className='h-[500px] lg:h-[800px] w-full bg-cover bg-left'
        style={{ backgroundImage: 'url(/assets/images/nostrasia-photo.jpg)' }}
      />

      <div className='bg-azure text-white py-20'>
        <Container className='flex flex-col xl:flex-row space-y-8 xl:space-y-0 xl:space-x-8'>
          <div className='max-w-prose space-y-6'>
            <h2 className='text-6xl font-semibold'>Believing is not enough</h2>
            <p className='text-2xl'>Don't let your memes be dreams. Let's work together to create the future that we believe in.</p>
            <Button theme='secondary' href='https://poast.tv/w/jUknj6g3BbkESsidKbDRzr'>Watch Video</Button>
          </div>

          <div className='rounded-xl overflow-hidden'>
            <video src='/assets/videos/believing.webm' controls />
          </div>
        </Container>
      </div>

      <Container className='flex justify-center'>
        <h1 className='my-16 sm:my-24 md:my-36 text-3xl sm:text-6xl md:text-7xl font-extrabold leading-tight md:leading-none text-center'>
          <span>I'm vegan</span>
          <br />
          <span className='text-azure'>by the way</span>.
        </h1>

        <div>
        </div>
      </Container>

      <Container>
        <div className='my-16 mx-auto text-center'>
          <img className='rounded-2xl mx-auto' src='/assets/vermin-supreme.jpg' />
        </div>
      </Container>

    </Layout>
  );
}

interface IStandee {
  src: string;
}

function Standee({ src }: IStandee) {
  return (
    <div className='flex justify-center'>
      <img
        className='mx-20 lg:pt-4 -z-40 left-auto right-auto bottom-0 xl:right-auto xl:bottom-auto xl:w-[700px] w-96'
        src={src}
      />
    </div>
  );
}
