import { IconExternalLink } from '@tabler/icons';

import Button from '../components/button';
import Container from '../components/container';
import Layout from '../components/layout';
import PageTitle from '../components/page-title';
import UnifiedMeta from '../components/unified-meta';
import Wrapper from '../components/wrapper';

export default function WorkPage() {
  return (
    <Layout>
      <UnifiedMeta
        title='Work | Alex Gleason'
        description='My Work'
      />
      <Container className='my-24 mb-36 space-y-24'>
        <WorkSection title='My Work'>
          <p className='text-xl'>
            I have been building social media and related ecosystems for over a decade.
            Here are a few highlights of my work.
          </p>
          <Button theme='secondary' href='https://gitlab.com/alexgleason' icon={<IconExternalLink size={20} />}>
            View on GitLab
          </Button>
        </WorkSection>

        <WorkSection title='Soapbox'>
          <img className='rounded-2xl' src='/assets/images/soapbox.png' />
          <p className='text-lg'>
            Soapbox is a frontend for the Fediverse with a focus on custom branding and ease of use.
            Used by millions of people around the world, perfect for building communties on decentralized platforms like Mastodon.
          </p>
          <Button theme='primary' href='https://soapbox.pub/' icon={<IconExternalLink size={20} />}>
            Learn More
          </Button>
        </WorkSection>

        <WorkSection>
          <div className='space-y-16'>
            <Project
              title='Mostr Bridge'
              description='Mostr is a bridge between Nostr and the Fediverse (Mastodon, ActivityPub, etc). It allows users on both networks to communicate, through a Mostr server.'
              link='https://soapbox.pub/blog/mostr-fediverse-nostr-bridge/'
            />
            <Project
              title='Ditto'
              description='Ditto is a Nostr server for building resilient communities online. With Ditto, you can create your own social network that is decentralized, customizable, and free from ads and tracking.'
              link='https://soapbox.pub/blog/announcing-ditto/'
            />
            <Project
              title='Nostrify'
              description='Framework for Nostr on Deno and web.'
              link='https://soapbox.pub/blog/announcing-nostrify/'
            />
            <Project
              title='Letr'
              description='Letr is a dynamic platform built on Nostr and where creators can share their content across various mediums, integrated with Lightning Network.'
              link='https://www.letr.co/'
            />
          </div>
        </WorkSection>

        <WorkSection title='Past Work'>
          <p className='text-lg'>
            I am <a className='underline' href='https://www.reuters.com/technology/head-engineering-trumps-truth-social-app-resigns-2023-07-17/' target='_blank'>formerly</a> Head of Engineering at Truth Social, which was built using&nbsp;Soapbox.
          </p>
        </WorkSection>
        
      </Container>
    </Layout>
  );
}

interface WorkSectionProps {
  title?: string;
  children: React.ReactNode;
}

function WorkSection({ title, children }: WorkSectionProps) {
  return (
    <Wrapper className='max-w-prose space-y-8'>
      {title && <PageTitle>{title}</PageTitle>}
      {children}
    </Wrapper>
  );
}

interface ProjectProps {
  title: string;
  description: string;
  link: string;
}

function Project({ title, description, link }: ProjectProps) {
  return (
    <div className='space-y-3'>
      <p className='text-2xl font-bold text-black'>{title}</p>
      <p className='text-lg'>
        {description}
      </p>
      <Button theme='primary' size='sm' href={link} icon={<IconExternalLink size={16} />}>
        Learn More
      </Button>
    </div>
  );
}


