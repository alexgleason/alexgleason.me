# alexgleason.me

Personal website for Alex Gleason.
Designed to look like a politican's website, even though I'm not a politician.

[https://alexgleason.me](https://alexgleason.me/)

## Technology stack

This website is built with React, TypeScript, and Next.js, then exported to static HTML, CSS, and JS.
React is a client-side framework, but Next.js is a hybrid approach which allows us to build pages in React that ultimately don't require JavaScript in the user's browser.

- [React](https://reactjs.org/) to design the pages.
- [Next.js](https://nextjs.org/) to export the code into HTML/CSS/JS.
- [Tailwind](https://tailwindcss.com/) as a utility CSS framework.
- [Contentlayer](https://www.contentlayer.dev/) to connect Next.js with our local Markdown files.
- [MDX](https://mdxjs.com/) for rich content in blog posts and other pages.

## Local development

You will need to install Node.js and [Yarn](https://yarnpkg.com/) (hint: `npm install -g yarn`), then:

```sh
# Do this once to install dependencies
yarn

# Run the local development environment
yarn dev
```

## Deployment

This site is deployed automatically.

When code is merged into the `main` branch, GitLab CI builds a new version of the site and deploys it to GitLab Pages.

## License

© Alex Gleason  

alexgleason.me is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

alexgleason.me is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with alexgleason.me. If not, see <https://www.gnu.org/licenses/>.